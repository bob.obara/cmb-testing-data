import smtk
import sys

# Entry point (main export function)
#import smtk
#m = smtk.attribute.Manager()
#r = smtk.util.AttributeReader()
#l = smtk.util.Logger()
#r.read(m, '/home/acbauer/CODE/PREPROCESS/CMB/Source/TestingData/SimBuilderGUI/expressions.sbi', l) # should return false
#

def checkFirst(attribute, name, number_of_values):
    return_val = 0
    if attribute.name() != name:
        print "ERROR: name should be ", name, " but is ", attribute.name()
        return_val = 1

    group_item = smtk.attribute.to_concrete(attribute.item(0))
    X = smtk.attribute.to_concrete(group_item.find('X'))
    if X.numberOfValues() != number_of_values:
        print "ERROR: first.X should have", number_of_values, "values but has ", X.numberOfValues()
        return_val = 1
    if X.value(2) != 0.0:
        print "ERROR: first.X.value(2) should be 0. but is ", X.value(2)
        return_val = 1
    if X.value(3) != 4.:
        print "ERROR: first.X.value(3) should be 4. but is ", X.value(3)
        return_val = 1
    Value = smtk.attribute.to_concrete(group_item.find('Value'))
    if Value.numberOfValues() != number_of_values:
        print "ERROR: first.Value should have", number_of_values, "values but has ", Value.numberOfValues()
        return_val = 1
    if Value.value(3) != 0.0:
        print "ERROR: first.Value.value(3) should be 0 but is ", Value.value(3)
        return_val = 1
    if Value.value(5) != 9.:
        print "ERROR: first.Value.value(5) should be 9. but is ", Value.value(5)
        return_val = 1

    return return_val

def checkThird(attribute):
    return_val = 0
    group_item = smtk.attribute.to_concrete(attribute.item(0))
    X = smtk.attribute.to_concrete(group_item.find('X'))
    Value = smtk.attribute.to_concrete(group_item.find('Value'))
    if X.numberOfValues() != 6 or Value.numberOfValues() != 6:
        print "ERROR: third should have 6 values but has ", X.numberOfValues(), Value.numberOfValues()
        return_val = 1
    for i in xrange(X.numberOfValues()):
        if X.value(i) != i*2+1:
            print "ERROR: third.X.value(", i, ") should be ", i*2+2, " but is ", X.value(i)
            return_val = 1
        if Value.value(i) != (i*2+1)**2:
            print "ERROR: third.Value.value(", i, ") should be ", (i*2+1)**2, " but is ", Value.value(i)
            return_val = 1

    return return_val

def ExportCMB(spec):
    '''
    Entry function, called by CMB to test SMTK expression attributes
    '''
    manager = spec.getSimulationAttributes()
    return_val = 0

    if manager is None:
        print 'ERROR: No attribute manager found - no output generated'
        return False

    # check to see that the attributes we should have been made are there
    expression_atts = manager.findAttributes('PolyLinearFunction')

    attribute_names = []
    for att in expression_atts:
        if att.name() == "first":
            return_val = return_val + checkFirst(att, "first", 10)
            attribute_names.append(0)
        if att.name() == "second":
            # second is a copy of first, besides the name
            return_val = return_val + checkFirst(att, "second", 10)
            attribute_names.append(1)
        if att.name() == 'third':
            return_val = return_val + checkThird(att)
            attribute_names.append(2)
        if att.name() == "fourth":
            return_val = return_val + checkFirst(att, "fourth", 11)
            attribute_names.append(3)
        if att.name() == "fifth":
            return_val = return_val + checkFirst(att, "fifth", 9)
            attribute_names.append(4)

    attribute_names.sort()
    if attribute_names != range(5):
        print 'ERROR: Missing some expression attributes ', attribute_names
        return_val = 1

    # check if we can reference an attribute
    att = manager.findAttribute('Expression test')
    d = smtk.attribute.to_concrete(att.item(0))
    if not d.isExpression(0):
        print 'ERROR: Load Curve should have an expression'
        return_val = 1

    if d.expression(0).name() != "fifth":
        print 'ERROR: Load Curve has wrong expression'
        return_val = 1

#    if return_val:
#        sys.exit(1)

    print 'finished'

    return True
