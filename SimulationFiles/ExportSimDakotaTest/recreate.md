ExportSimDakota

To recreate test:
File-Open simplebox.cmb from testing-data
File-Open dakota_example.sbt from testing-data
Switch to Attribute tab
Set "Method List" to "test"
Select "Method" tab
Select the "Nondeterministic Sampling Method"
Set "Method Name" to "test"
Check "Random Number Generator"
Select "Variables" Tab
Click New. Set Variable Name to "aa"
File-Export Simulation File
Check "Enable multiple Selection"
Unselect and reselect Dakota
 * There is a bug where Dakota is not really selected
 * Doing this actually selects Dakota
Select "dakotaExporter.py" from testing-data as "Python Script"
Save to test.bc in the testing-data folder.
 * Note: Manually edit the script afterward to point to testing/Temporary
Export and Stop Recording
